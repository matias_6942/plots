
import platform
import json
from pprint import pprint
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import pandas as pd
import numpy as np
import seaborn as sns

# Libraries

print('Python: ' + platform.python_version())
print('Matplotlib: ' + matplotlib.__version__)
print('Pandas: ' + pd.__version__)

# Plots over time

def drawDataOnTime(df, time):
    """Dibuja dos columnas de un dataframe a lo largo del tiempo y guarda la figura"""

    createFig(timeUnit=time)
    ax1 = df.Humedad.plot(color='green', grid=True, label='Humedad', marker='o')
    ax2 = df.Temperatura.plot(color='red', grid=True, secondary_y=True, label='Temperatura', marker='o') 
    createAxis(ax1, ax2)
    if time == "días":
        plt.savefig('HumTemp_'+ time +'.png', bbox_inches='tight', dpi=300)
    return

def createFig(timeUnit):
    """Crea gráfico que permite dibujar en una cierta unidad de tiempo"""

    plt.figure(figsize=(12.80, 7.20))
    plt.title('Gráfico de Humedad y Temperatura en el Tiempo')
    plt.xlabel('Tiempo [' + timeUnit +']')
    return

def createAxis(ax1, ax2):
    """Nombra a los ejes Y del gráfico y los señala en la leyenda"""

    ax1.set_ylabel('Humedad [%]')
    ax2.set_ylabel('Temperatura [°C]')
    h1, l1 = ax1.get_legend_handles_labels()
    h2, l2 = ax2.get_legend_handles_labels()
    plt.legend(h1+h2, l1+l2, loc=2)	
    return

def drawHourlyHumTemp(df):
    """Dibuja dos columnas de un dataframe a lo largo de las horas"""
    
    points = 50
    i = 0
    while i < len(df.index):
        j = i + points
        aux_df = df[i:j]
        drawDataOnTime(aux_df, 'horas')
        plt.savefig('HumTemp_'+ str(df.index[i]) +'.png', bbox_inches='tight', dpi=300)
        plt.show()
        i = j
    return

# Plots for Data Analysis

def drawScatter(df1, df2):
    df = df1.join(df2)
    errors = df.std()
# Con barras de error, se ve mal
#	ax = df.plot.scatter(x='Humedad', y='Temperatura', title = 'Correlación entre Humedad y Temperatura', grid=False, color='c', figsize=(12.80, 7.20), 
#		yerr= errors.Temperatura, xerr= errors.Humedad)
    ax = df.plot.scatter(x='Humedad', y='Temperatura', title = 'Correlación entre Humedad y Temperatura', grid=False, color='c', figsize=(12.80, 7.20))
    ax.set_xlabel('Humedad [%]')
    ax.set_ylabel('Temperatura [°C]')
    plt.savefig('HumTemp_Correlation'+'.png', bbox_inches='tight', dpi=300)
    plt.show()
    return

def analogReadToPercentage(analogRead):
    highestVal = 780.
    lowerVal = 373.
    highestPerc = 100.
    lowerPerc = 0.
    m = (highestVal-lowerVal)/(lowerPerc-highestPerc)
    percentage = m*(analogRead-lowerVal) + highestPerc
    if (percentage < 0):
        percentage = 0.
    if (percentage > 100):
        percentage = 100.      
    return percentage

def drawLinearReg(df1, df2):
    df = df1.join(df2)	
    ax = sns.regplot(x=df.columns[1], y=df.columns[0], data=df, y_jitter=.2, color='c')
    #ax.set_xticks(tempList)
    ax.grid(True)
    ax.set_title('Correlación entre Humedad y Temperatura con Regresión Lineal')
    plt.savefig('HumTemp_Correlation'+'.png', bbox_inches='tight', dpi=300)
    plt.show()
    return