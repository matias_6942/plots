
from buildPlots import * 

"""Los dataframes de pandas se crean con listas."""

# Load, parse and draw JSON data

with open('invernaderopedro-export.json') as f:
    data = json.load(f)

    humList = []
    tempList = []
    timestampList = []
    dateList = []
    dateTimeList = []

    for key, value in data["logs"].items():
        aux = value.split(" ")
        if len(value.split(" ")) == 5:
            humList.append(float(aux[0]))
            tempList.append(float(aux[1]))
            timestampList.append(aux[2])
            dateList.append(aux[4])
            dateTimeList.append(aux[4]+ ' ' + aux[2])

df1 = pd.DataFrame({'Humedad': humList})
df2 = pd.DataFrame({'Temperatura': tempList})
df = df1.join(df2)
df.index = pd.to_datetime(dateTimeList, format='%d/%m/%Y %H:%M:%S')

print("\n Cantidad de datos a graficar: " + str(len(df.index)) + "\n")

#drawDataOnTime(df, 'días')
drawHourlyHumTemp(df)
#drawScatter(df1, df2)
#drawLinearReg(df1, df2)

print("\n Gráficos Guardados! \n")